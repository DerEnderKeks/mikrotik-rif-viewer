import {createApp} from 'vue'
import PrimeVue from 'primevue/config';
import 'primevue/resources/themes/aura-dark-lime/theme.css'
import 'primeflex/primeflex.css'
import 'primeicons/primeicons.css'

import 'highlight.js/styles/stackoverflow-dark.css'
import 'highlight.js/lib/common';
import hljsVuePlugin from '@highlightjs/vue-plugin'

import './style.css'
import App from './App.vue'

const app = createApp(App)
app.use(PrimeVue)
app.use(hljsVuePlugin)

app.mount('#app')
